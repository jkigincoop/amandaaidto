﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmandaDTOs.Mail
{
    /// <summary>
    /// Our mail attribute.  Used to pass specific values to the controller.
    /// </summary>
    public class MailAttribute
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
