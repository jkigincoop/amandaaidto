﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmandaDTOs.Mail
{
    public class HydratedMailModel :MailModel
    {
        public IEnumerable<MailTransmissionAttempts> TransmissionHistory { get; set; }

        public HydratedMailModel Archive { get; set; }
    }
}
