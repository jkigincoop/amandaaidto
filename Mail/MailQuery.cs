﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmandaDTOs.Mail
{
    public class MailQuery
    {
        /// <summary>
        /// Statuses to pull
        /// </summary>
        public IEnumerable<Mail.MailModel.MailStatus> Statuses { get; set; }

        /// <summary>
        /// Only pull the simplified data, no sending units etc.
        /// </summary>
        public bool SimplifiedModelOnly { get; set; }

        /// <summary>
        ///  The earliest date to get
        /// </summary>
        public DateTime? LowFilter { get; set; }

        /// <summary>
        /// The highest date to get
        /// </summary>
        public DateTime? HighFilter { get; set; }

        /// <summary>
        /// Indicates whether or not we get archived emails (no by default)
        /// </summary>
        public bool GetArchived { get; set; }

        /// <summary>
        /// Gets by date, reversed.
        /// </summary>
        public bool OrderByDate { get; set; }

        /// <summary>
        /// The maximum number of entities to pull
        /// </summary>
        public int? Take { get; set; }

        /// <summary>
        /// The number to skip
        /// </summary>
        public int? Skip { get; set; }

        /// <summary>
        /// The to email
        /// </summary>
        public string ToEmail { get; set; }

        /// <summary>
        /// The sending application
        /// </summary>
        public string SendingApplication { get; set; }
    }
}
