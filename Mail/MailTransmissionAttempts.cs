﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmandaDTOs.Mail
{
    /// <summary>
    /// Historical records of transmitted mail
    /// </summary>
    public class MailTransmissionAttempts
    {
        public long Id { get; set; }
        public long EmailId { get; set; }
        public DateTime Date { get; set;}
        public int StatusCode { get; set; }
        public string Comment { get; set; }

    }
}
