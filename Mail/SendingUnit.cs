﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace AmandaDTOs.Mail
{
    /// <summary>
    /// The sending unit, combines the email address with the type of sending.
    /// </summary>
    public class SendingUnit
    {
        public enum SendingType { To=0, Cc=1, Bcc=2};
        public MailAddress EmailAddress { get; set; }
        public SendingType Routing { get; set; }
    }
}
