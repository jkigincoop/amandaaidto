﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mail;

namespace AmandaDTOs.Mail
{
    /// <summary>
    /// Our mail model.
    /// </summary>
    public class MailModel
    {
       public enum MailStatus { InProcess=0, Delivered=1, Errored=2, Archived=3, PermanentlyErrored=4, Processing=5 }
       public long Id { get; set; }
       public bool IsArchived { get; set; }
       public string UserName { get; set; }
       public string Password { get; set; }
       public IEnumerable<SendingUnit> To { get; set; }
       public MailAddress From { get; set; }
       public bool IsHtml { get; set; }
       public string Body { get; set; }
       public string Subject { get; set; }
       public DateTime ArrivalDate { get; set; }
        public DateTime? DateSent { get; set; }
        public IEnumerable<string> AttachmentFileLocations { get; set; }
       public IEnumerable<MailAttribute> Attributes { get; set; }
       public MailStatus CurrentState { get; set; }
       public string CorrelationId { get; set; }
       public string SendingApplication { get; set; }

       public long? ArchiveId { get; set; }
        public HydratedMailModel ToHydratedMailModel()
        {
            return new HydratedMailModel()
            {
                ArrivalDate = this.ArrivalDate,
                AttachmentFileLocations = this.AttachmentFileLocations,
                Attributes = this.Attributes,
                Body = this.Body,
                CorrelationId = this.CorrelationId,
                CurrentState = this.CurrentState,
                From = this.From,
                Id = this.Id,
                IsArchived = this.IsArchived,
                IsHtml = this.IsHtml,
                Password = this.Password,
                SendingApplication = this.SendingApplication,
                Subject = this.Subject,
                To = this.To,
                UserName = this.UserName
            };
        }
    }
}
